from django.urls import path

from .views import (
    api_list_auto_sales,
    api_list_customers,
    api_list_sales_people,
    api_show_auto_sales,
    api_show_customer,
    api_show_sales_person,
)

urlpatterns = [
    path("customers/", api_list_customers, name="api_create_customers"),
    path("customers/<int:pk>/", api_show_customer, name="api_show_customer"),
    path("sales_people/", api_list_sales_people, name="api_create_sales_people"),
    path("sales_people/<int:pk>/", api_show_sales_person, name="api_show_sales_person"),
    path("auto_sales/", api_list_auto_sales, name="api_create_auto_sales"),
    path("auto_sales/<int:pk>/", api_show_auto_sales, name="api_show_auto_sales"),
    path("automobile/<int:automobile_vo_id>/auto_sales/", api_list_auto_sales, name="api_list_auto_sales"),
]
