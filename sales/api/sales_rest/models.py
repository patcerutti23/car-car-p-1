from django.db import models
from django.urls import reverse


class AutomobileVO(models.Model):
    vin = models.CharField(max_length=17, unique=True)
    sold = models.BooleanField(default=False)


class SalesPerson(models.Model):
    employee_name = models.CharField(max_length= 150)
    employee_number = models.CharField(max_length=200, unique=True)

    def get_api_url(self):
        return reverse("api_show_sales_people", kwargs={"pk": self.pk})



class Customer(models.Model):
    customer_name = models.CharField(max_length=150)
    address = models.CharField(max_length=250)
    phone_number = models.CharField(max_length=200)

    def get_api_url(self):
        return reverse("api_show_customers", kwargs={"pk": self.pk})


class AutoSale(models.Model):
    price = models.SmallIntegerField()

    sales_person = models.ForeignKey(
        SalesPerson,
        related_name="sales_people",
        on_delete=models.PROTECT,
    )
    customer = models.ForeignKey(
        Customer,
        related_name="customers",
        on_delete=models.PROTECT,
    )
    automobile = models.OneToOneField(
        AutomobileVO,
        related_name="automobiles",
        on_delete=models.PROTECT,
    )

    def get_api_url(self):
        return reverse("api_show_auto_sales", kwargs={"pk": self.pk})
