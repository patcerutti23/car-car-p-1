from django.contrib import admin

# Register your models here.
from .models import AutomobileVO, SalesPerson, Customer, AutoSale

@admin.register(AutomobileVO)
class AutomobileVOAdmin(admin.ModelAdmin):
    pass

@admin.register(SalesPerson)
class SalesPersonAdmin(admin.ModelAdmin):
    pass

@admin.register(Customer)
class CustomerAdmin(admin.ModelAdmin):
    pass


@admin.register(AutoSale)
class AutoSaleAdmin(admin.ModelAdmin):
    pass
