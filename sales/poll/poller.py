import django
import os
import sys
import time
import json
import requests

sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "sales_project.settings")
django.setup()

# Import models from sales_rest, here.
from sales_rest.models import AutomobileVO

def get_vin():
    response = requests.get("http://inventory-api:8000/api/automobiles/")
    content = json.loads(response.content)
    for auto in content["autos"]:
        AutomobileVO.objects.update_or_create(
            vin= auto["vin"],
            defaults={"vin": auto["vin"], "sold": auto["sold"]},
        )

def poll():
    while True:
        print('Sales poller polling for data')
        try:
            get_vin()
            print("success")
            pass
        except Exception as e:
            print("error", e, file=sys.stderr)
        time.sleep(60)


if __name__ == "__main__":
    poll()
