from django.shortcuts import render
from common.json import ModelEncoder
from .models import Service, Technician, AutomobileVO
from django.views.decorators.http import require_http_methods
from django.http import JsonResponse
import json



class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = ["vin",]


class TechnicianListEncoder(ModelEncoder):
    model = Technician
    properties = ["technician_name", "employee_number", "id"]

@require_http_methods(["GET", "DELETE"])
def api_show_technician(request, pk):
    if request.method == "GET":
        show_technician = Technician.objects.get(id=pk)
        return JsonResponse(
            show_technician,
            encoder=TechnicianListEncoder,
            safe=False,
        )
    else:
        count, _= Technician.objects.filter(id=pk).delete()
        return JsonResponse({"delete": count > 0})

@require_http_methods(["GET", "POST"])
def api_list_technicians(request):
    if request.method == "GET":
        technician = Technician.objects.all()
        return JsonResponse(
            {"technicians": technician},
            encoder=TechnicianListEncoder
        )
    else:
        try:
            content = json.loads(request.body)
            create_technician = Technician.objects.create(**content)
            return JsonResponse(
                create_technician,
                encoder=TechnicianListEncoder,
                safe=False,
            )
        except Technician.DoesNotExist:
            return JsonResponse (
                {"message": "Does Not Exist"},
                status=400,
            )


class ServiceEncoder(ModelEncoder):
    model = Service
    properties = [
        "vin",
        "owner",
        "appointment",
        "reason",
        "finished",
        "technician",
        "id",
    ]

    encoders = {"technician": TechnicianListEncoder()}

    def get_extra_data(self, o):
        count = AutomobileVO.objects.filter(vin=o.vin).count()
        return {"vip": count > 0}



@require_http_methods(["GET", "DELETE", "PUT"])
def api_show_service(request, pk):
    if request.method == "GET":
        service = Service.objects.get(id=pk)
        return JsonResponse(
            service,
            encoder=ServiceEncoder,
            safe=False,
        )
    elif request.method == "PUT":
        Service.objects.filter(id=pk).update(finished=True)
        appointment = Service.objects.get(id=pk)
        return JsonResponse(
            appointment,
            encoder=ServiceEncoder,
            safe=False,
        )

    else:
        try:
            count, _= Service.objects.filter(id=pk).delete()
            return JsonResponse({"deleted": count > 0})
        except Service.DoesNotExist:
            return JsonResponse({"message": "Does Not Exist"})

@require_http_methods(["GET", "POST"])
def api_list_services(request):
    if request.method == "GET":
        services = Service.objects.all()
        return JsonResponse(
            {"services": services},
            encoder=ServiceEncoder,
        )
    else:
        content = json.loads(request.body)
        try:
            technician = Technician.objects.get(id=content["technician"])
            content["technician"] = technician
        except Technician.DoesNotExist:
            return JsonResponse(
                {"message": "Does Not Exist"},
                status=400,
            )
        appointment = Service.objects.create(**content)
        return JsonResponse(
            appointment,
            encoder=ServiceEncoder,
            safe=False,
        )


@require_http_methods(["GET"])
def get_service_history(request):
    history = Service.objects.filter(finished=True)
    return JsonResponse(
        {"history": history},
        encoder=ServiceEncoder,
    )
