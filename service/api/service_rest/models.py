from django.db import models
from django.urls import reverse


class AutomobileVO(models.Model):
    vin = models.CharField(max_length=17, unique=True)
    import_href = models.CharField(max_length=200)
    vip = models.BooleanField(default=False)


class Technician(models.Model):
    technician_name = models.CharField(max_length=200)
    employee_number = models.CharField(max_length=50)

def get_api_url(self):
    return reverse("api_show_technician", kwargs={"pk": self.pk})


class Service(models.Model):
    vin = models.CharField(max_length=17)
    owner = models.CharField(max_length=200)
    appointment = models.DateTimeField(null=True)
    reason = models.CharField(max_length=200)
    finished = models.BooleanField(default=False)
    technician = models.ForeignKey(
        Technician,
        related_name="services",
        on_delete=models.CASCADE,
    )


def __str__(self):
        return self.owner

def get_api_url(self):
        return reverse("api_show_service", kwargs={"pk": self.pk})
