from django.urls import path
from .views import api_show_technician, api_list_technicians, api_show_service, api_list_services, get_service_history


urlpatterns = [

    path("services/", api_list_services, name="api_create_service"),
    path("services/<int:pk>/", api_show_service, name="api_show_service"),
    path("technician/<int:pk>/", api_show_technician, name="api_show_technician"),
    path("technician/", api_list_technicians, name="api_create_technician"),
    path("services/history/", get_service_history, name="api_show_history"),
]
