import React from 'react';
import { NavLink } from 'react-router-dom'

class AppointmentList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            services: [],
        }
    }

    async componentDidMount() {
        const url = "http://localhost:8080/api/services/";
        const response = await fetch(url)
        if (response.ok) {
            const data = await response.json();
            console.log(data)
            this.setState({ services: data.services })
        }
    }

    async handle(id, action) {
        const finished = JSON.stringify({ "finished": true});
        const url = `http://localhost:8080/api/services/${id}/`;
        const fetchOptions = {
            method: action,
            body: finished,
            headers: {
                'Content-Type': 'application/json'
            },
        };
        const response = await fetch (url, fetchOptions);
        if (response.ok && action === "delete") {
            this.setState ({
                services: this.state.services.filter(service => service.id !== id)
            })
            return;
        }
        if (response.ok && action === "put") {
            this.setState ({
                services: this.state.services.filter(service => service.id !== id)
            })
            return;
        }
    }

    render() {
        return (
            <>
            <div>
                    <button type="button" className="btn btn-success" style={{marginTop: "15px", marginBottom: "15px"}}>
                        <NavLink to="/appointment/new" className="link-info text-white" style={{textDecoration: "none"}}> Create A New Service</NavLink>
                    </button>
                    <h1>Service Appointments</h1>
                    <table className="table table-striped">
                        <thead>
                        <tr>
                            <th>VIP</th>
                            <th>VIN#</th>
                            <th>Customer name</th>
                            <th>Date</th>
                            <th>Time</th>
                            <th>Technician</th>
                            <th>Reason</th>
                        </tr>
                        </thead>
                        <tbody>
                        {this.state.services.map(service => {
                        const dateTime = new Date(service.appointment)
                        return (
                        <tr key={service.vin}>
                            <td>{ JSON.stringify(service.vip)}</td>
                            <td>{ service.vin }</td>
                            <td>{ service.owner }</td>
                            <td>{ dateTime.toLocaleDateString() }</td>
                            <td>{ dateTime.toLocaleTimeString([], { hour: '2-digit', minute: '2-digit' }) }</td>
                            <td>{ service.technician.technician_name }</td>
                            <td>{ service.reason }</td>
                            <td>
                            <button style={{borderRadius: 0}} onClick={() => this.handle(service.id, "delete")} className="btn btn-danger">Cancel</button>
                            <button style={{borderRadius: 0}} onClick={() => this.handle(service.id, "put")} className="btn btn-success">Finished</button>
                            </td>
                        </tr>
                        );
                    })}
                    </tbody>
                </table>
            </div>
        </>
        );
    }
}
export default AppointmentList;
