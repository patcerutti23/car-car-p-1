import React from 'react';

class AutomobileList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            models: [],
            name: '',
            manufacturer: '',
            picture_url: '',

        }
    }

    async componentDidMount() {
        const url = 'http://localhost:8100/api/models/';
        const response = await fetch(url)
        if (response.ok) {
            const data = await response.json();
            this.setState({ models: data.models })
        }
    }

    render() {
        return (
        <>
            <div className="my-3 container">
                <h1>Vehicle Models</h1>
                <table className="table table-striped">
                    <thead>
                    <tr>
                        <th>Name</th>
                        <th>Manufacturer</th>
                        <th>Picture</th>
                    </tr>
                    </thead>
                    <tbody>
                    {this.state.models.map(model => {
                        return (
                        <tr key={model.id}>
                            <td>{ model.name }</td>
                            <td>{ model.manufacturer.name }</td>
                            <td><div style={{maxHeight: '180px', contain: 'content'}}><img className="img-thumbnail rounded img-responsive" src={model.picture_url} style={{maxWidth: '330px', height: 'auto'}}/></div></td>
                        </tr>
                        );
                    })}
                    </tbody>
                </table>
            </div>
        </>
        );
    }
}
export default AutomobileList;
