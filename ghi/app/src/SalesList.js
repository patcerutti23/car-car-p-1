import React from 'react';
import { NavLink } from 'react-router-dom';


class SalesList extends React.Component {
    constructor (props) {
        super(props)
        this.state = {
        salesRecord: [],
        }
    }
    async componentDidMount() {
        const url = 'http://localhost:8090/api/auto_sales/'
        let response = await fetch(url)

        if (response.ok) {
            let data = await response.json()

            this.setState({salesRecord: data.auto_sales })

        }
    }


    render () {
        return (
            <>
                <div>
                    <button className="btn btn-primary" type="button">
                        <NavLink to="CreateSaleForm" className="link-info">Create New Sale</NavLink>
                    </button>
                    <h1>Sales List</h1>
                    <table className="table table-striped">
                        <thead>
                            <tr>
                                <th>Automobile</th>
                                <th>Sales Person</th>
                                <th>Customer</th>
                                <th>Price</th>
                            </tr>
                        </thead>
                        <tbody>
                            {this.state.salesRecord.map(sale => {
                                return (
                                    <tr key={sale.href}>
                                        <td>{sale.automobile.vin}</td>
                                        <td>{sale.sales_person.employee_name}</td>
                                        <td>{sale.customer.customer_name}</td>
                                        <td>{sale.price}</td>
                                    </tr>
                                )
                            })}
                        </tbody>
                    </table>
                </div>
            </>
        )
    }
}

export default SalesList;
