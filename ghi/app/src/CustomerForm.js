import React from 'react';


class CustomerForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
        customerName: '',
        address: '',
        phoneNumber: '',
        customers: [],
        }
    }

    handleSubmit = async (event) => {
        event.preventDefault();
        const data = {
        customer_name: this.state.customerName,
        address: this.state.address,
        phone_number: this.state.phoneNumber
        };


        const customerUrl = 'http://localhost:8090/api/customers/';
        const fetchOptions = {
        method: "POST",
        body: JSON.stringify(data),
        headers: {
        'Content-Type': 'application/json'
        },
        };

        const customerResponse = await fetch(customerUrl, fetchOptions);
        if (customerResponse.ok) {
        const newCustomer = await customerResponse.json();

        this.setState({
            customerName:'',
            address: '',
            phoneNumber: '',
        });
        }

    }
        handleCustomerNameChange = (event) => {
            const value = event.target.value;
            this.setState({customerName: value})
    }
        handleAddressChange = (event) => {
            const value = event.target.value;
            this.setState({address: value})
    }
        handlePhoneNumberChange = (event) => {
            const value = event.target.value;
            this.setState({phoneNumber: value})
    }
    render() {
        return (
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>Add New Customer</h1>
                        <form onSubmit={this.handleSubmit} id="create-customer-form">
                            <div className="form-floating mb-3">
                                <input onChange={this.handleCustomerNameChange} value={this.state.customerName} placeholder="Name" required type="text" name="customerName" id="customerName" className="form-control" />
                                <label htmlFor="name">Customer Name</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={this.handleAddressChange} value={this.state.address} placeholder="Name" required type="text" name="address" id="address" className="form-control" />
                                <label htmlFor='name'>Address</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={this.handlePhoneNumberChange} value={this.state.phoneNumber} placeholder="Name" required type="text" name="phoneNumber" id="phoneNumber" className="form-control" />
                                <label htmlFor='name'>Phone Number</label>
                            </div>
                                <button className='btn btn-lg btn-primary'>Create</button>
                        </form>
                    </div>
                </div>
            </div>
        )
    }
}

export default CustomerForm;
