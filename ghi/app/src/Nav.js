
import { NavLink } from 'react-router-dom';

function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-success" style={{overflow: 'auto'}}>
      <div className="container-fluid">
        <NavLink className="navbar-brand" to="/">CarCar</NavLink>
        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
            <li className="nav-link">
              <NavLink className="nav-link" to="/manufacturers/">Manufacturers</NavLink>
            </li>
            <li className="nav-link">
              <NavLink className="nav-link" to="/manufacturers/new">Create A Manufacturer</NavLink>
            </li>
            <li className="nav-link">
              <NavLink className="nav-link" to="/models/">Vehicle Models</NavLink>
            </li>
            <li className="nav-link">
              <NavLink className="nav-link" to="/models/new">Create Vehicle Model</NavLink>
            </li>
            <li className="nav-link">
              <NavLink className="nav-link" to="/automobiles/">Automobile List</NavLink>
            </li>
            <li className="nav-link">
              <NavLink className="nav-link" to="/automobiles/new">Create Automobile</NavLink>
            </li>
            <li className="nav-link">
            <NavLink className="nav-link" to="SalesPersonForm">Create Sales Person Form</NavLink>
            </li>
            <li className="nav-link">
            <NavLink className="nav-link" to="CustomerForm">Create Customer Form</NavLink>
            </li>
            <li className="nav-link">
            <NavLink className="nav-link" to="SalesList">List of Sales</NavLink>
            </li>
            <li className="nav-link">
              <NavLink className="nav-link" to="/technician/">Add A Technician</NavLink>
            </li>
            <li className="nav-link">
              <NavLink className="nav-link" to="/appointment/">List of Appointments</NavLink>
            </li>
            <li className="nav-link">
              <NavLink className="nav-link" to="/appointment/new">Schedule An Appointment</NavLink>
            </li>
            <li className="nav-link">
              <NavLink className="nav-link" to="/appointment/history">Service History</NavLink>
            </li>
            <li className="nav-link">
              <NavLink className="nav-link" to="SaleForm">New Sale Form</NavLink>
            </li>
            <li className="nav-link">
              <NavLink className="nav-link" to="/SalesHistoryList">Sales History List</NavLink>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  )
}

export default Nav;
