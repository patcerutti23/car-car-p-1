import React from 'react';

class AutomobileForm extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            manufacturers: [],
            name: '',
            manufacturer_id: '',
            picture_url: '',
        }
        this.handleSubmit = this.handleSubmit.bind(this)
        this.handleName = this.handleName.bind(this)
        this.handlePicture = this.handlePicture.bind(this)
        this.handleManufacturer = this.handleManufacturer.bind(this)
    }

    async componentDidMount () {
        const url = "http://localhost:8100/api/manufacturers/";
        const response = await fetch (url);
        if (response.ok) {
            const data = await response.json();
            this.setState({ manufacturers: data.manufacturers })
        }
    }

    handleSubmit = async (event) => {
        event.preventDefault();
        const data = { ...this.state }
        delete data.manufacturers
        const url = "http://localhost:8100/api/models/";
        const fetchOptions = {
            method: 'post',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            },
        };

        const response = await fetch (url, fetchOptions);
        if (response.ok) {
            this.setState({
                name: '',
                picture_url: '',
                manufacturer_id: '',
            })
        }
    }
    handleName = (event) => {
        const value = event.target.value;
        this.setState({ name: value })
    }
    handlePicture = (event) => {
        const value = event.target.value;
        this.setState({ picture_url: value })
    }
    handleManufacturer = (event) => {
        const value = event.target.value;
        this.setState({ 'manufacturer_id': value })
    }


    render() {
        return (
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>Create A Vehicle Model</h1>
                        <form onSubmit={this.handleSubmit} id="create-model-form">
                            <div className="form-floating mb-3">
                                <input onChange={this.handleName} value={this.state.name} placeholder="Name" required type ="text" name="name" id="name" className="form-control" maxLength="50" />
                                <label htmlFor="name">Name</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={this.handlePicture} value={this.state.picture_url} placeholder="Name" required type ="text" name="picture_url" id="picture_url" className="form-control" />
                                <label htmlFor="name">Picture URL</label>
                            </div>
                            <div className="form-floating mb-3">
                                <select onChange={this.handleManufacturer} value={this.state.id} placeholder="Name" required type ="text" name="manufacturer" id="manufacturer" className="form-control">
                                <option value="">Choose A Manufacturer</option>
                                {this.state.manufacturers.map(make => {
                                    return (
                                        <option key={make.id} value={make.id}> {make.name}</option>
                                    )
                                })}
                                </select>
                            </div>
                            <button className="btn btn-primary btn-lg">Create</button>
                        </form>
                    </div>
                </div>
            </div>
        )
    }
}
export default AutomobileForm;
